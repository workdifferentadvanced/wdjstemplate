By c948229 @ Telstra.com
-Latest update: 2017Aug15 v2.05.

----HOW TO RUN-----
0. You must have internet connection..
1. Download this latest version from bitbucket. "clone" or "pull".
2. You must change any usernames / passwords / IP's / Links in this application to suite your own environment. Seach for "pass" etc.
3. CLI into this main / root directory. Run "npm install".
4. CLI "node index.js". To start this application.
5. Also run "node-red-start" (Optional).

AS THIS IS A TEAM EFFORT. IF YOU MODIFY / IMPROVE. PLEASE UPDATE BACK TO GITBUCKET REPO. IF CONFIDENT.

If any app / install fails. Always prepend command with "sudo"!!!
--------------------
(Optional) install "npm i nodemon" and run "nodemon index.js" to start.
-Need to manually start this app plus Node-Red.
---------History-------------
2017Jul v0.01 - Started.
2017Jul23 v1.00- Added LCD x 6 functions and Splunk API now works with feedback to GUI.
2017Aug13 v2.04 - Cleaned up code a little so ready to release to #WorkDifferent team.
2017Aug15 v2.05 - Uploaded to bitbucket repo. Minor typo fixes.
-------Known Bugs------------
-Pinging bug.
-Need to reset application before next LCD  app will work is displayed / triggered. i.e. only way to refresh LCD is by restart app. Not good. Need to clear variables etc.
-------------------------------
https://shkspr.mobi/blog/2012/07/3g-internet-on-raspberry-pi-success/
sudo ./sakis3g --interactive 
sudo ./sakis3g connect info
----------------------------
This is for splunk Forwarder.
#! /bin/sh
for c in $(seq 1 60)
do
TIME=$(date +"%D %T.%3N %Z")
TEMP=$(vcgencmd measure_temp)
VOLTS=$(vcgencmd measure_volts)
#CPU_TEMP='echo $TEMP | cut -c 6-9'
#VOLTS='echo $VOLTS | cut -c 6-9'
echo "time=$TIME|volts=$VOLTS|temp=$TEMP"
sleep 1
done
-------------------------------
Have installed on Pi so far..(use either "apt" or "npm")
-NodeJS (Latest)
-Update all / most Raspberry softwaqre to the latest "apt updatePi.
-nodemon
-Make sure Pi has access to interet to download Bootstrap etc.
-Davfs2 = Box.com (Optional)
sudo mount -o uid=pi -o gid=pi -t davfs https://dav.box.com/dav /home/pi/pi_on_box
sudo dpkg-reconfigure davfs2
sudo umount -t davfs https://dav.box.com/dav /home/pi/pi_on_box
-------------------------------
NEED MORE HELP?
GOOGLE IS YOU FRIEND!!
-----------------------------
IF YOU COPY / REVERSE ENGINEER ANY ONE'S CODE.
PLEASE GIVE CREDIT WHERE CREDIT IS DUE.
SIMPLY ADD A REFERENCE LINK SOMEWHERE IN YOUR NEW CODE. (This also Helps documentation / explain code.)