/*
#WorkDifferent. Template v2.041.

This is for training purpaoses only.
If you copy code from others please add links, source and give credit where credits due e.g.

Credit to..
Source http://codeacadamy.com/blah

There is at least one main bug in here, see if you can find it hint: route "/pinging/:id"

If you learn something new. Please inform the rest of #WorkDifferent team on Yammer/Sharepoint.

I've tried not to use EcmaScript2015 (ES6) as much as possble so newbies can understand code easier.
This is more about learning than correct coding at this point i.e. is not the most efficent way to do things yet.

*/
var express = require('express');
var socket = require('socket.io');
var exphbs = require('express-handlebars');
var path = require('path');
var ping = require('ping');
var fs = require("fs");
var nedb = require('nedb');
var router = express.Router();
var expressValidator = require('express-validator'); //Not used.
var bodyParser = require('body-parser');
var splunkjs = require('splunk-sdk');
var Lcd = require('lcd');
//var lcd;
var moment = require('moment');
//var moment = require('moment-timezone');
//Moment().tz('Australia/Sydney').format();

//Setup LCD globals.
//Source..
//http://thejackalofjavascript.com/rpi-16x2-lcd-print-stuff/
const lcd = Lcd({
  rs: 12,
  e: 21,
  data: [5, 6, 17, 18],
  cols: 8,
  rows: 2
});

//Setup Splunk connect global variables.
    var username = opts.username    || "admin";
    var password = opts.password    || "your_splunk_pass_here_default_changeme";
    var scheme   = opts.scheme      || "https";
    var host     = opts.host        || "your_splunk_ip_or_hostname_here";
    var port     = opts.port        || "8089";
    var version  = opts.version     || "default";
    
    var service = new splunkjs.Service({
        username: username,
        password: password,
        scheme: scheme,
        host: host,
        port: port,
        version: version
    });

 var os = require( 'os' );
 var networkInterfaces = os.networkInterfaces( );
//console.log(networkInterfaces.wlan0[0].address);

    lcd.on('ready', function() {
      lcd.setCursor(0, 0);
      //lcd.autoscroll();
      var text = networkInterfaces.wlan0[0].address;
      print("" + text + "");
    });

//lcd.blink();
//-----------LCD---------------------
router.get('/lcdprint', function(req, res) {
 fs.readFile(path + 'lcdprint.handlebars', 'utf8', function(err, contents) {
    if (err) {
      return console.log(err);
    }
   console.log("Contents: ",contents);
    res.send(contents);
  });
});


router.get('/print/:text', function(req, res) {
  var text = req.params.text;
  //var text = req.body.text;
 
  lcd.clear();
  lcd.setCursor(0, 0);
 
  print(decodeURI(text));
 
  res.send({
    status: true,
    text: text
  });
});



var timer;
 
function print(str, pos) {
 
  pos = pos || 0;
 
  timer = setTimeout(function() {
    if (pos === str.length - 1) {
      clearTimeout(timer);
      str = '';
    } else {
      print(str, pos + 1);
    }
  }, 300);
 
  lcd.print(str[pos]);
}
//------------END LCD PRINT--------------------



// App setup
var app = express();
var server = app.listen(4000, function(){
    console.log('listening for requests on port 4000,');
});

//app.set('views', path.join(__dirname, 'views'));
var path = __dirname + '/views/';

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Static files
//app.use(express.static('public'));
app.use(express.static('views'));
app.use('/', router);
//===============================================
// instruct the app to use the `bodyParser()` middleware for all routes
app.use(bodyParser());

//Not used.
app.use(expressValidator());


// This route receives the posted form.
// As explained above, usage of 'body-parser' means
// that `req.body` will be filled in with the form elements
app.post('/contactsnew', function(req, res){
  var firstname = req.body.firstname;
  var phone = req.body.phone;
  var html = 'Hello: ' + firstname + ' ' + phone + '.<br>' +'<a href="/contacts">Try again.</a>';
  res.send(html);

  people = [{
                firstName: firstname,
                phone1: phone
            }];

  contacts.insert(people, function(err, docs) {  
    docs.forEach(function(d) {
        console.log('Saved user:', d);
    });
  });

});
//===============================================
// Socket setup & pass server
var io = socket(server);
io.on('connection', (socket) => {
    console.log('made socket connection', socket.id);
    // Handle chat event
    socket.on('chat', function(data){
        // console.log(data);
        io.sockets.emit('chat', data);
    });
    // Handle typing event
    socket.on('typing', function(data){
        socket.broadcast.emit('typing', data);
    });
});


router.get('/home', function(req,res){
    res.render(path + 'home', {
        people: people
    });
});

router.get('/chat', function(req,res){
    res.render(path + 'chat');
});

app.get('/about', function(req,res){
    res.render(path + 'about');
});

router.get('/contactsnew', function(req,res){
    res.render(path + 'contactsnew');
});

router.get('/contacts', function(req,res){
    res.render(path + 'contacts',{
        people:people
    });
});


router.get('/ping', function(req,res){
    res.render(path +'ping');
});

router.get('/splunk', function(req, res){
	res.render(path + 'splunk/index');
});


//---------SPLUNK LIST APPS--------------------
router.get('/splunk1', function(req, res){
  console.log("Begin get Splunk Apps.");
	//res.render(path + 'splunk/helloworld/index');
var http = require('http');
var results =[];
//var exphbs = require('express-handlebars');

//////exports.main = function(opts, done) {
  // This is just for testing - ignore it
  //////  opts = opts || {};
    

     // First, we log in
      service.login(function(err, success) {
          // We check for both errors in the connection as well
          // as if the login itself failed.
          if (err || !success) {
              console.log("Error in logging in");
              done(err || "Login failed");
              return;
          } 
          
          // Now that we're logged in, let's get a listing of all the apps.
          service.apps().fetch(function(err, apps) {
              if (err) {
                  console.log("There was an error retrieving the list of applications:", err);
                  done(err);
                  return;
              }
              
              var appList = apps.list();
              console.log("Applications:");
              for(var i = 0; i < appList.length; i++) {
                  var app = appList[i];
                  //myapps += appList[i];
                  console.log("  App " + i + ": " + app);
                  results.push(app);
              } 
              res.render(path + 'mysplunk/apps',{
                 myapps: results
               });
             ////// done();
          });
      });
});//End of splunk.

//-------------------SPLUNK -----------------
router.get('/splunkjobs', function(req, res){
  var results = [];
  //var splunkjs = require('splunk-sdk');

  var Async  = splunkjs.Async;

  //exports.main = function(opts, callback) {
      // This is just for testing - ignore it
   //   opts = opts || {};

      var sid;

      Async.chain([
              // First, we log in
              function(done) {
                  service.login(done);
              },
              // Perform the search
              function(success, done) {
                  if (!success) {
                      done("Error logging in");
                  }
                  
                  service.search("search index=_internal | head 1", {}, done);
              },
              function(job, done) {
                  // Store the sid for later use
                  sid = job.sid;
                  console.log("Created a search job with sid: " + job.sid);
                  done();
              }
          ],
          function(err) {
              if (err || !sid) {
                  if (err.hasOwnProperty("data") && err.data.hasOwnProperty("messages")) {
                      console.log(err.data.messages[0].text);
                  }
                  else {
                      console.log(err);
                  }
                  if (!sid) {
                      console.log("Couldn't create search.");
                  }
                  callback(err);
              }
              else {
                  Async.chain([
                          function(done) {
                              // Since we have the job sid, we can get that job directly
                              service.getJob(sid, done);
                          },
                          function(job, done) {
                              console.log("Got the job with sid: " + job.sid);
                              results.push(job.sid);
                              res.render(path + 'mysplunk/jobs',{
                                  myapps: results
                              });
                              done();
                          }
                      ]//,
                      // function(err) {
                      //     callback(err);
                      // }
                  );
              }
          }
      );
  //};
});
//-------------------------SPLUNK FIRE ALERTS------------------------------
router.get('/splunkalerts', function(req, res){
  var results = [];

    // First, we log in.
    service.login(function(err, success) {
        // We check for both errors in the connection as well
        // as if the login itself failed.
        if (err || !success) {
            console.log("Error in logging in");
            done(err || "Login failed");
            return;
        } 

        // Now that we're logged in, let's get a listing of all the fired alert groups
        service.firedAlertGroups().fetch(function(err, firedAlertGroups) {
            if (err) {
                console.log("ERROR", err);
                done(err);
                return;
            }

            // Get the list of all fired alert groups, including the all group (represented by "-")
            var groups = firedAlertGroups.list();
            console.log("Fired alert groups:");

            var listGroupCallback = function(err, firedAlerts, firedAlertGroup) {
                // How many times was this alert fired?
                console.log(firedAlertGroup.name, "(Count:", firedAlertGroup.count(), ")");
                // Print the properties for each fired alert (default of 30 per alert group)
                for(var i = 0; i < firedAlerts.length; i++) {
                    var firedAlert = firedAlerts[i];
                    for(var key in firedAlert.properties()) {
                        if (firedAlert.properties().hasOwnProperty(key)) {
                           console.log("\t", key, ":", firedAlert.properties()[key]);
                           results.push(key);
                             res.render(path + 'mysplunk/alerts',{
                                  myapps: results
                              });
                        }
                    }
                    console.log();
                }
                console.log("======================================");
            };

            for(var a in groups) {
                if (groups.hasOwnProperty(a)) {
                    var firedAlertGroup = groups[a];
                    firedAlertGroup.list(listGroupCallback);
                }
            }

           // done();
        });
    });
//};

});

// if (module === require.main) {
//     exports.main({}, function() {});
// }

//---------------------------------SPLUNK LIST SAVED SEARCHES -----------------------------------------
router.get('/splunksearches', function(req, res){
  var results = [];
    // First, we log in
    service.login(function(err, success) {
        // We check for both errors in the connection as well
        // as if the login itself failed.
        if (err || !success) {
            console.log("Error in logging in");
            done(err || "Login failed");
            return;
        }
        
        // Now that we're logged in, let's get a listing of all the saved searches.
        service.savedSearches().fetch(function(err, searches) {
            if (err) {
                console.log("There was an error retrieving the list of saved searches:", err);
                done(err);
                return;
            }
            
            var searchList = searches.list();
            console.log("Saved searches:");
            for(var i = 0; i < searchList.length; i++) {
                var search = searchList[i];
                results.push(search);
                console.log("  Search " + i + ": " + search.name);
                console.log("    " + search.properties().search);
            } 
            res.render(path + 'mysplunk/searches',{
              myapps : results
            });
            
            //done();
        });
    });
});
//-----------------------------------------------------------------------------------------------------
router.get('/pinging/:id', function(req, res){
    console.log(req.params);
    //res.send(200);
    //res.send(req.params.id, 200);
    var gethost = req.params.id;
    //var hosts = ['localhost', 'google.com', 'yahoo.com'];
    var hosts = [gethost];
    var results;
    hosts.forEach(function(host){
        ping.sys.probe(host, function(isAlive){
            var msg = isAlive ? 'host ' + host + ' is alive' : 'host ' + host + ' is dead';
            console.log(msg);
            //var results = msg;
            // res.render('pingresults', {
            //     results : msg
            // });
            //res.sendFile(path + 'pingresults.html');
            //res.render('pingresults');
            //results = results + msg;
            //////res.send("Results: " + msg);
            //res.send()
            res.render(path + 'ping', {
                results : msg
            });
        });
    });
});

router.get('/help', function(req,res){
    res.render(path +'help');
});



// var templateData = {
//    year: new Date().getFullYear(),
//    article: 'templates will be right in the<code> code</code>folder'
// };
//--------------LCD STUFF--------------------

router.get('/lcdtime', function(req,res){
  var results = [];

      // lcd = new Lcd({
      //   rs: 12,
      //   e: 21,
      //   data: [5, 6, 17, 18],
      //   cols: 8,
      //   rows: 2
      // });

    lcd.on('ready', function() {
      setInterval(function() {
        lcd.setCursor(0, 0);
        var now = Date();
       lcd.print(moment(new Date()).toString().substring(16, 24))}, 1000);
    });

    results.push(Date().toString().substring());
    // If ctrl+c is hit, free resources and exit.
    process.on('SIGINT', function() {
      lcd.clear();
      lcd.close();
      process.exit();
    });
    
   res.render('lcdtime', {
       myapps : results
   });
});


//--------------LCD Template----------------------
router.get('/lcdscroll', function(req,res){
  var results = [];
     //Lcd = require('lcd'),
       //var
      //   lcd = new Lcd({
      //   rs: 12,
      //   e: 21,
      //   data: [5, 6, 17, 18],
      //   cols: 16,
      //   rows: 1
      // });

    //lcd.clear();
    lcd.on('ready', function() {
      lcd.setCursor(16, 0);
      lcd.autoscroll();
      var text = 'Telstra #WorkDifferent - Hello, World!...';
      print(text);
      results.push(text);
      console.log("LCD Scolling text.")
      console.log("Results: ", results);
      res.render('lcdscroll', {
           myapps : results
      });

    });
     
    function print(str, pos) {
      pos = pos || 0;
     
      if (pos === str.length) {
        pos = 0;
      }
     
      lcd.print(str[pos]);
     
      setTimeout(function() {
        print(str, pos + 1);
      }, 300);
    }
     
    // If ctrl+c is hit, free resources and exit.
    process.on('SIGINT', function() {
      lcd.clear();
      lcd.close();
      process.exit();
    });    
});

//-------------------
router.get('/lcdipaddress', function(req,res){
  var results = [];
     //Lcd = require('lcd'),
       //var
      //   lcd = new Lcd({
      //   rs: 12,
      //   e: 21,
      //   data: [5, 6, 17, 18],
      //   cols: 16,
      //   rows: 1
      // });

    //lcd.clear();
    lcd.on('ready', function() {
      lcd.setCursor(16, 0);
      lcd.autoscroll();
      var text = networkInterfaces.wlan0[0].address;
      print("My IP Address : " + text + "...      ");
      results.push(text);
      //console.log("LCD Scolling text.")
      //console.log("Results: ", results);
      res.render('lcdipaddress', {
           myapps : results
      });

    });
     
    function print(str, pos) {
      pos = pos || 0;
     
      if (pos === str.length) {
        pos = 0;
      }
     
      lcd.print(str[pos]);
     
      setTimeout(function() {
        print(str, pos + 1);
      }, 300);
    }
     
    // If ctrl+c is hit, free resources and exit.
    process.on('SIGINT', function() {
      lcd.clear();
      lcd.close();
      process.exit();
    });    
});
//-----------------------------------------------------

router.get('/lcdtwolines', function(req,res){
    lcd.clear();
    lcd.on('ready', function() {
    console.log("Hellowwww");
  });

  res.end();
});
//-------------------
router.get('/lcdstockticker', function(req,res){
  var results = [];
    var http = require('http');

    function getQuote(stock) {
      http.get({
        host: 'www.google.com',
        port: 80,
        path: '/finance/info?client=ig&q=' + stock
      }, function(response) {
        response.setEncoding('utf8');
        var data = "";

        response.on('data', function(chunk) {
          data += chunk;
        });

        response.on('end', function() {
          if (data.length > 0) {
            try {
              var data_object = JSON.parse(data.substring(3));
            } catch (e) {
              return;
            }

            var quote = {};
            quote.ticker = data_object[0].t;
            quote.exchange = data_object[0].e;
            quote.price = data_object[0].l_cur;
            quote.change = data_object[0].c;
            quote.change_percent = data_object[0].cp;
            quote.last_trade_time = data_object[0].lt;
            quote.dividend = data_object[0].div;
            quote.yield = data_object[0].yld;

            console.log(JSON.stringify(quote, null, 4));

            lcd.clear();
            lcd.setCursor(16, 0);
            lcd.autoscroll();
            var text = "Real-time stock price : " + quote.ticker + " , Price : " + quote.price + " , Change(%) : " + quote.change_percent + " ** ";
            print(text);
            results.push(text);
          }

          res.render('lcdstockticker', {
              myapps : results
          });


        });
      });
    }

    function print(str, pos) {

      pos = pos || 0;

      if (pos === str.length) {
        pos = 0;
      }

      lcd.print(str[pos]);

      setTimeout(function() {
        print(str, pos + 1);
      }, 300);
    }

    // If ctrl+c is hit, clear, free resources and exit.
    process.on('SIGINT', function() {
      lcd.clear();
      lcd.close();
      process.exit();
    });

    //getQuote('goog');
    getQuote('asx:tls');
    //res.end();
});


router.get('/stop', function(req,res){
  res.send('closing..');
  //process.exit();
  //lcd.clear();
  lcd.close();
  //lcd = 0;
  //res.end();
});
//--------------------
//End of Router
//==========================================
var people = [];
//var myapps = [];
//========================================
var contacts = new nedb({ filename: __dirname + '/contacts.db', autoload: true });

contacts.find({}, function(err, docs) {  
    docs.forEach(function(d) {
        //console.log('Found user:', d);
        people.push(d);
        //console.log("People: ", people);
    });
});
//=================================================
var methods = {
    timestamp: function() {
        console.log('Current Time in Unix Timestamp: ' + Math.floor(Date.now() / 1000));
    },
    currentDate: function() {
        console.log('Current Date is: ' + new Date().toISOString().slice(0, 10));
    }
};


 module.exports = methods;

//--------------------TWITTER------------------------
//https://dzone.com/articles/how-to-use-twitter-api-using-nodejs
//hosts.forEach(function(host){

router.get('/twitsearch', function(req,res){
  var results =[];
  //var results;
  console.log("Example is up now..")
  var Twit = require('twit');
  var config = require('./config')
  var T = new Twit(config);
  var params = { 
  //q: 'akshay',
   //count 100
   count: 1000,
   q: '#Telstra'
  }

  T.get('search/tweets', params,searchedData);

   function searchedData(err, data, response) {
    //results.push(data);
     //results.forEach(function(output){
        console.log("Output: ", data['statuses'][0]['text']);
        var output = data['statuses'][0];
        results.push(output);
     // });
      //console.log(data);
      res.render(path + 'twitsearch', {
       myapps : results
        });
    };

});
//---------------POST TWEET-----------------
router.get('/twitpost', function(req, res){
var yourtweet = "Hello World - Ignore - Automation test 4!"
var results=[];
console.log("Example is up now..")
var Twit = require('twit');
var config = require('./config')
var T = new Twit(config);
var tweet = { 
  status: yourtweet }

  T.post('statuses/update', tweet, tweeted)

   function tweeted(err, data, response) {
    if(err){
  console.log("Something went wrong!");
  }
  else{
  console.log("Voila It worked!");
  results.push(yourtweet);
  //results = yourtweet;
  res.render(path + 'twitpost', {
    myapps : results
  });

  }
}

//res.end();

});
//--------------------------------------
router.get('/twitschedule', function(req, res){
  var results = [];
  console.log("Example is up now..")
  var Twit = require('twit');
  var config = require('./config')
  var T = new Twit(config);


  setInterval(tweetScheduler,1000*20);
  tweetScheduler();
  function tweetScheduler(){
  var randomNumber = Math.floor(Math.random()*1000);
  var yourtweet = randomNumber + "#Automation Test - Ignore."
  var tweet = { 
  status: yourtweet }

  T.post('statuses/update', tweet, tweeted)

   function tweeted(err, data, response) {
    if(err){
  console.log("Something went wrong!");
  }
  else{
  console.log("Voila It worked!");
     results.push(yourtweet);
    //results = yourtweet;
    res.render(path + 'twitschedule', {
      myapps : results
    });
  }
}
}

});